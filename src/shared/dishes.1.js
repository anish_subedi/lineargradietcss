export const DISHES = [
  {
    id: 0,
    color: ["#ff5f52", "#82ffa1"],
    title: "001 Chitwan sunset",
    background: { background: "linear-gradient(90deg, #ff5f52, black)" }
  },
  {
    id: 1,
    color: ["#5425da", "#82ffa1"],
    title: "001 Chitwan sunset",
    background: { background: "linear-gradient(90deg, green, #82ffa1)" }
  },
  {
    id: 2,
    color: ["#f52f5f", "#82ffa1"],
    title: "001 Chitwan sunset",
    background: { background: "linear-gradient(90deg, #5425da, #82ffa1)" }
  },
  {
    id: 3,
    color: ["#f52f5f", "#82ffa1"],
    title: "001 Chitwan sunset",
    background: { background: "linear-gradient(90deg, #5425da, #82ffa1)" }
  },
  {
    id: 5,
    color: ["#ff5f52", "#82ffa1"],
    title: "001 Chitwan sunset",
    background: { background: "linear-gradient(90deg, #ff5f52, blue)" }
  },
  {
    id: 6,
    color: ["#5425da", "#82ffa1"],
    title: "001 Chitwan sunset",
    background: { background: "linear-gradient(90deg, yellow, #82ffa1)" }
  },
  {
    id: 7,
    color: ["#f52f5f", "#82ffa1"],
    title: "001 Chitwan sunset",
    background: { background: "linear-gradient(90deg, #5425da, #82ffa1)" }
  },
  {
    id: 8,
    color: ["#f52f5f", "#82ffa1"],
    title: "001 Chitwan sunset",
    background: { background: "linear-gradient(90deg, #5425da, #82ffa1)" }
  }
];
