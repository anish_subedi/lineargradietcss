import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { Card, CardText, CardBody, CardTitle, Button } from "reactstrap";
import "../App.css";
import "../App1.css";
import { DISHES } from "../shared/Dishes";

class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dishes: DISHES,
      selecteditemid: null
    };
    // this.copyItem = this.copyItem.bind(this);
  }

  // copyItem = (event, itemsid) => {
  //   alert(itemsid);
  //   return this.setState({
  //     selecteditemid: itemsid
  //   });
  // };
  render() {
    const list = this.state.dishes.map(item => {
      const classes = `setup ${item.divs}`;
      return (
        <Container key={item.id}>
          <Card className="card">
            <Col className={classes} style={item.background} />
            <CardBody className="cardbody">
              <CardTitle>{item.title}</CardTitle>

              <CardText>
                {item.color[0]} --> {item.color[1]}{" "}
              </CardText>
              <Button onClick={event => this.copyItem(event, item.id)}>
                Copy CSS
              </Button>
              <Button>GET .PNG</Button>
            </CardBody>
          </Card>
        </Container>
      );
    });

    return (
      <Container>
        <Row className="divstyle">{list}</Row>
      </Container>
    );
  }
}

export default Demo;
